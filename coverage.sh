#!/bin/sh -eu

python3 -m coverage run --omit='*_t.py,test.py,dbfs/testing.py' ./test.py -v
python3 -m coverage report
python3 -m coverage html
