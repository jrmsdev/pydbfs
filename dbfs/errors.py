# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import errno

class DBFSError (OSError):
    def __init__ (self, e, m):
        self.name = errno.errorcode[e]
        self.msg = m
        self.errno = int (e)

    def __str__ (self):
        return '%s: %s' % (self.name, self.msg)

    def __repr__ (self):
        return '<DBFSError.%s>' % str (self)

EFAULT = lambda m: DBFSError (errno.EFAULT, m)
ENODEV = lambda m: DBFSError (errno.ENODEV, m)
ENOENT = lambda m: DBFSError (errno.ENOENT, m)
