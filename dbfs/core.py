# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import logging
from os import path

from dbfs.libw.fusew import FuseWrapper
from dbfs.storage.storage import DBFSStorage

log = logging.getLogger ('dbfslog').getChild (__name__)

class DBFS (object):

    def __init__ (self, args):
        self.args = args
        if not hasattr (self.args, 'storage'):
            raise RuntimeError ('storage arg not provided')
        if not hasattr (self.args, 'mountpoint'):
            self.__set_mountpoint ()
        log.debug ('storage spec: %s', self.args.storage)
        log.debug ('mountpoint: %s', self.args.mountpoint)
        self.storage = DBFSStorage (self.args.storage)

    def __set_mountpoint (self):
        self.args.mountpoint = path.abspath (self.args.storage)

    def mount (self):
        with self.storage as s:
            return FuseWrapper (self.args.mountpoint, s).main (self.args.debug)
