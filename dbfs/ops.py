# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import logging

log = logging.getLogger ('dbfslog').getChild (__name__)

class ReaderOps (object):

    async def dbfs_getattr (self, path):
        log.debug ('dbfs_getattr: %s', path)
        return dict ()

class DBFSOps (ReaderOps):

    def __init__ (self, storage):
        log.debug (repr (storage))
        self.storage = storage

    async def dbfs_init (self, path):
        log.debug ('dbfs_init: %s', path)
        return None

    async def dbfs_destroy (self, path):
        log.debug ('dbfs_destroy: %s', path)
        return None
