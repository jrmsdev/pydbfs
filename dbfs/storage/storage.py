# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import logging
from os import path

from dbfs import task, errors

log = logging.getLogger ('dbfslog').getChild (__name__)

class StorageContext (object):

    # storage open
    def __enter__ (self):
        log.debug (self.spec)
        task.run (self.open)
        return self

    # storage close
    def __exit__ (self, exc_type, exc, exc_tb):
        log.debug (self.spec)
        log.debug (f'Exception: {exc_type} {exc} {exc_tb}')
        if self.is_open ():
            task.run (self.close)
        if exc is not None:
            raise RuntimeError ('unhandled exception') from exc
        return self

class DBFSStorage (StorageContext):

    def __init__ (self, storage_spec):
        log.debug ('DBFSStorage spec: %s', storage_spec)
        self.spec = storage_spec
        self.__is_open = False

    def is_open (self):
        return self.__is_open

    async def open (self):
        log.debug (self.spec)
        self.__is_open = True
        # FIXME
        # ~ raise errors.ENOENT (self.__spec)

    async def close (self):
        log.debug (self.spec)
        self.__is_open = False
