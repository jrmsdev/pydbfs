# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import logging

from dbfs.libw import xfusepy

log = logging.getLogger ('dbfslog').getChild (__name__)

class FuseWrapper (object):

    def __init__ (self, mountpoint, storage):
        log.debug ('FuseWrapper')
        self.mpoint = mountpoint
        self.storage = storage
        self.libfuse = xfusepy
        log.debug (self.libfuse.__name__)

    def main (self, debug):
        log.debug ('main')
        log.debug ('libfuse.main')
        return self.libfuse.main (self.mpoint, self.storage, debug)
