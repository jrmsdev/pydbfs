# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import sys
import logging

log = logging.getLogger ('dbfslog').getChild (__name__)

try:
    import fuse
except ModuleNotFoundError:
    print ('ERROR: fuse module not found - try: pip install fusepy')
    sys.exit (1)
except OSError as err:
    print ('ERROR:', err)
    sys.exit (1)

from fuse import FUSE
from dbfs.libw.xfusepy_ops import XFusepyOps

def main (mountpoint, storage, debug):
    log.debug (mountpoint)
    try:
        FUSE (XFusepyOps (storage), mountpoint, foreground = debug)
    except RuntimeError as err:
        return err.args[0]
    return 0
