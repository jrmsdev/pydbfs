# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import logging

from fuse import Operations, LoggingMixIn
from dbfs import task
from dbfs.ops import DBFSOps

log = logging.getLogger ('dbfslog').getChild (__name__)

class XFusepyOps (LoggingMixIn, Operations, DBFSOps):

    def __init__ (self, storage):
        log.debug (repr (storage))
        super ().__init__ (storage)

    # DEBUG:fuse.log-mixin:-> init / ()
    # DEBUG:fuse.log-mixin:<- init None

    def init (self, path):
        log.debug ('init: %s', path)
        return task.run (self.dbfs_init, path)

    # DEBUG:fuse.log-mixin:-> destroy / ()
    # DEBUG:fuse.log-mixin:<- destroy None

    def destroy (self, path):
        log.debug ('destroy: %s', path)
        return task.run (self.dbfs_destroy, path)

    # READ OPS

    # DEBUG:fuse.log-mixin:-> getattr / (None,)
    # DEBUG:fuse.log-mixin:<- getattr {'st_mode': 16877, 'st_nlink': 2}

    def getattr(self, path, fh=None):
        log.debug ('getattr: %s %s', path, fh)
        return task.run (self.dbfs_getattr, path)

# DEBUG:fuse.log-mixin:-> opendir / ()
# DEBUG:fuse.log-mixin:<- opendir 0

# DEBUG:fuse.log-mixin:-> statfs / ()
# DEBUG:fuse.log-mixin:<- statfs {}

# DEBUG:fuse.log-mixin:-> readdir / (0,)
# DEBUG:fuse.log-mixin:<- readdir ['.', '..']

# DEBUG:fuse.log-mixin:-> releasedir / (0,)
# DEBUG:fuse.log-mixin:<- releasedir 0
