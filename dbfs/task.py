# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import asyncio
import logging
import multiprocessing as mp

log = logging.getLogger ('dbfslog').getChild (__name__)

class Task (object):

    def __init__ (self, funcw):
        self.__funcw = funcw
        self.__ret = -128

    def run (self):
        loop = asyncio.new_event_loop ()

        try:
            log.debug ('create task')
            loop.create_task (self.__main (loop))
            loop.run_forever ()
        finally:
            pending = asyncio.Task.all_tasks (loop)
            log.debug (f'Pending tasks: {pending}')
            loop.close ()

        if len (pending) > 0:
            raise RuntimeError (f'pending tasks: {pending}')

        log.debug (f'return: {self.__ret}')
        return self.__ret

    async def __main (self, loop):
        self.__ret = await self.__funcw ()
        log.debug ('loop stop')
        loop.stop ()

def __runtask (q, l, funcw):
    log.debug ('new task process')
    l.acquire ()
    try:
        q.put (Task (funcw).run ())
    finally:
        l.release ()

def run (func, *args):
    async def funcw ():
        log.debug (f'async {func}')
        return await func (*args)
    ctx = mp.get_context ()
    lock = ctx.Lock ()
    q = ctx.Queue ()
    p = ctx.Process (target = __runtask, args = (q, lock, funcw))
    p.start ()
    ret = q.get ()
    p.join ()
    return ret
