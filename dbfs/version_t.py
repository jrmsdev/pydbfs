# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

from dbfs.testing import TestCase
from dbfs import version

class T (TestCase):

    def test_appname (self):
        self.assertEqual (version.APPNAME, 'dbfs')

    def test_version (self):
        self.assertEqual (version.VERSION, f'{version.VMAJOR}.{version.VPATCH}')

    def test_string (self):
        self.assertEqual (f'{version.APPNAME} v{version.VERSION}', version.string ())
