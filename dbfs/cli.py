# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import os
import sys
import argparse
import logging

from dbfs import version
from dbfs.errors import DBFSError
from dbfs.cmd import mount, listfs

log = logging.getLogger ('dbfslog').getChild (__name__)

# docs.p.o/3.6/howto/logging.html#changing-the-format-of-displayed-messages
LOG_FORMAT = '[%(process)s %(thread)s] %(name)s [%(funcName)s] %(message)s'

def parse_args (parser, argv):
    parser.add_argument ('-d', '--debug', action = 'store_true', default = False)

    subparser = parser.add_subparsers (description = None)
    mount.add_menu (subparser)
    listfs.add_menu (subparser)

    args = parser.parse_args (argv)
    if not hasattr (args, 'cmd'):
        args.cmd = None

    enable_debug (args)
    return args

def main (argv = None):
    check_root ()
    parser = argparse.ArgumentParser (prog = 'dbfs')
    args = parse_args (parser, argv)
    if args.cmd is None:
        parser.print_help ()
        return 1
    try:
        return args.cmd (args)
    except DBFSError as err:
        log.error (err)
        return err.errno

def system_mount (argv = None):
    check_root ()
    parser = argparse.ArgumentParser (prog = 'mount_dbfs')
    parser.add_argument ('-d', '--debug', action = 'store_true', default = False)
    parser.add_argument ('storage')
    parser.add_argument ('mountpoint')
    args = parser.parse_args (argv)
    enable_debug (args)
    try:
        return mount.mount_cmd (args)
    except DBFSError as err:
        log.error (err)
        return err.errno

def check_root ():
    if 0 in (os.getuid (), os.geteuid ()):
        print ('ERROR: should not run as root')
        sys.exit (1)

import platform

def enable_debug (args):
    if args.debug:
        logging.basicConfig (level = logging.DEBUG, format = LOG_FORMAT)
        os.environ['PYTHONASYNCIODEBUG'] = '1'
        # some debug logging
        log.debug (version.string ())
        log.debug (platform.platform ())
        log.debug ('Python v%s %s', platform.python_version (),
                                    ' '.join (platform.python_build ()))
        log.debug (platform.python_compiler ())
        log.debug ('%s %s', platform.python_implementation (),
                            ' '.join (platform.libc_ver ()))
        log.debug (sys.thread_info)
        log.debug (f'UID: {os.getuid()} GID: {os.getgid()}')
        log.debug (f'EUID: {os.geteuid()} EGID: {os.getegid()}')
