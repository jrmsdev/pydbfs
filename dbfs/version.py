# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

APPNAME = 'dbfs'
VMAJOR = 0
VPATCH = 0
VERSION = f'{VMAJOR}.{VPATCH}'

def string ():
    return f'{APPNAME} v{VERSION}'
