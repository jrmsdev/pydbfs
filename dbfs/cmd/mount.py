# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import logging
from dbfs.core import DBFS

log = logging.getLogger ('dbfslog').getChild (__name__)

def add_menu (subparser):
    p = subparser.add_parser ('mount', help = 'mount a filesystem')
    p.add_argument ('storage', help = 'file system storage')
    p.set_defaults (cmd = mount_cmd)

def mount_cmd (args):
    log.debug (args)
    if not hasattr (args, 'storage'):
        log.error ('no storage')
        return 1
    return DBFS (args).mount ()
