#!/usr/bin/env python3

# Copyright (c) Jeremías Casteglione <jrmsdev@gmail.com>
# See LICENSE file.

import sys
from unittest import TextTestRunner, TestLoader

ldr = TestLoader()
suite = ldr.discover('./dbfs', '*_t.py')

def run (verbose = 1):
    if '-v' in sys.argv:
        verbose = 2
    rnr = TextTestRunner (verbosity=verbose)
    rst = rnr.run (suite)
    sys.exit (len (rst.errors) + len (rst.failures))

if __name__ == '__main__':
    run ()
